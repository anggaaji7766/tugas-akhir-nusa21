import dynamic from 'next/dynamic'

const PublicContainer = dynamic(() => import('layouts/containers/Public'))

const routes = [
  {
    path: '/',
    name: 'Home',
    navbar: true,
    layout: PublicContainer,
    exact: true,
  },
  {
    path: '/aboutus',
    name: 'About Us',
    navbar: true,
    layout: PublicContainer,
    exact: true,
  },
  {
    path: '/ourworks',
    name: 'Our Works',
    navbar: true,
    layout: PublicContainer,
    exact: true,
  },
  {
    path: '/blog',
    name: 'Blog',
    navbar: true,
    layout: PublicContainer,
    exact: true,
  },
  {
    path: '/event',
    name: 'Event',
    navbar: true,
    layout: PublicContainer,
    exact: true,
  },
  {
    path: '/contactUs',
    name: 'Contact Us',
    navbar: true,
    layout: PublicContainer,
    exact: true,
  },
]

export default routes
