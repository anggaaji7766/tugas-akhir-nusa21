import React from 'react'
import { Button } from 'antd'
import { useSwiper } from 'swiper/react'

interface Ibutton {
  icon?: string
  className: string
}


const ButtonSwiper = {
  ButtonSlideNext: (props: Ibutton) => {
    const { icon, className } = props
    const swiper = useSwiper()
    return (
      <Button className={className} onClick={() => swiper.slideNext()}>
        <img src={icon} alt="" />
      </Button>
    )
  },
  ButtonSlidePrev: (props: Ibutton) => {
    const { icon, className } = props
    const swiper = useSwiper()
    return (
      <Button className={className} onClick={() => swiper.slidePrev()}>
        <img src={icon} alt="" />
      </Button>
    )
  },
}

export default ButtonSwiper
